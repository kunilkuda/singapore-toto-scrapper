'''
MIT License

Copyright (c) 2017 Daniel Widyanto

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''
from flask import Flask
from flask import render_template

from datetime import time
import csv

from collections import OrderedDict

app = Flask(__name__)

@app.route("/")
def singtoto_chart():
    '''
    Create TOTO result chart from CSV file

    The CSV format:
    "date", winning-no-1, winning-no-2, winning-no-3, winning-no-4, winning-no-5, winning-no-6
    '''
    toto_result_filename = 'toto_result.csv' # Change this to suit your filename
    toto_result_dict = OrderedDict()

    with open(toto_result_filename) as csvfile:
        reader = csv.DictReader(csvfile, fieldnames=['daydate', 'win1', 'win2', 'win3', 'win4', 'win5', 'win6'])
        for row in reader:
            result_list = [row['win1'], row['win2'], row['win3'], row['win4'], row['win5'], row['win6'] ]
            toto_result_dict.update({row['daydate']: result_list})

    toto_result_histogram = {}
    for key, value in toto_result_dict.items():
        for win_no in value:
            if win_no in toto_result_histogram:
                toto_result_histogram[win_no] += 1
            else:
                toto_result_histogram[win_no] = 1

    toto_result_histogram = OrderedDict(sorted(toto_result_histogram.items(), key=lambda x: x[1], reverse=True))

    return render_template('singtoto_chart.html', result_histogram = toto_result_histogram, result_dict = toto_result_dict)


if __name__ == "__main__":
    app.run(debug=True)