'''
MIT License

Copyright (c) 2017 Daniel Widyanto

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''
from lxml import html

import cssselect
from cssselect import HTMLTranslator

import requests
from requests.exceptions import ReadTimeout

import time

toto_result_pages     = 'http://www.singaporepools.com.sg/DataFileArchive/Lottery/Output/toto_result_draw_list_en.html'
toto_result_page_list = []

def get_result_page_list():
    '''
    The Singapore Pool's website has list of HTML pages for each TOTO draw. 
    Download and create a list to scrap later.
    '''
    base_html = 'http://www.singaporepools.com.sg/en/product/sr/Pages/toto_results.aspx?'

    page = requests.get(toto_result_pages)
    tree = html.fromstring(page.content)
    nodes = tree.xpath('//select/option')

    for node in nodes:
        toto_result_page_list.append(base_html + node.attrib['querystring'])


def start_singapore_toto_scrap():
    '''
    Scrap TOTO winning numbers from Singapore TOTO website
    '''
    headers = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0'}

    get_result_page_list()
    
    # Prepare the CSS selector
    drawDate = HTMLTranslator().css_to_xpath('.toto-result .drawDate')
    win1 = HTMLTranslator().css_to_xpath('.toto-result .win1')
    win2 = HTMLTranslator().css_to_xpath('.toto-result .win2')
    win3 = HTMLTranslator().css_to_xpath('.toto-result .win3')
    win4 = HTMLTranslator().css_to_xpath('.toto-result .win4')
    win5 = HTMLTranslator().css_to_xpath('.toto-result .win5')
    win6 = HTMLTranslator().css_to_xpath('.toto-result .win6')

    for result_page in toto_result_page_list:
        try:
            page = requests.get(result_page, timeout=10)
        except ReadTimeout:
            print("%s TIMEOUT" % result_page)
            continue # Skip to the next page

        print("%s %d" % (result_page, page.status_code))
        #if (page.status_code != 200):
        #    print("%s %d" % (result_page, page.status_code))
        #    continue # Skip to the next page

        tree = html.fromstring(page.content)

        drawDate_list = tree.xpath(drawDate + '/text()')
        win1_list = tree.xpath(win1 + '/text()')
        win2_list = tree.xpath(win2 + '/text()')
        win3_list = tree.xpath(win3 + '/text()')
        win4_list = tree.xpath(win4 + '/text()')
        win5_list = tree.xpath(win5 + '/text()')
        win6_list = tree.xpath(win6 + '/text()')

        # You can convert this to JSON, CSV, etc..but this demo will just print the results
        print("%s : %s %s %s %s %s %s" % ("".join(drawDate_list), 
                                          "".join(win1_list), 
                                          "".join(win2_list), 
                                          "".join(win3_list), 
                                          "".join(win4_list),
                                          "".join(win5_list), 
                                          "".join(win6_list)))

        # The DDoS prevention algo inside sgpool webserver prevent the data to be downloaded that fast
        time.sleep(1)   # delays for 1 seconds. 


if __name__ == '__main__':
    start_singapore_toto_scrap()