# Singapore TOTO Result Scraper
This simple software will download all of Singpaore TOTO's result (within singaporepool website) for further analysis. The software comes with simple chart demo (histogram and history of winning numbers).

## Installation

    $ mkvirtualenv singtoto_scrap
    $ git clone git@gitlab.com:kunilkuda/singapore-toto-scrapper.git
    $ cd singapore-toto-scrapper
    $ pip install -r requirements.txt

## Usage

To download results from singaporepool's website

    $ python3 singtoto_scrap.py > toto_result.csv

To view the chart of the downloaded result

    $ export FLASK_APP = singtoto_chart.py
    $ python3 -m flask run --host=0.0.0.0

Then, open your browser and enter the URL: http://127.0.0.1:5000
